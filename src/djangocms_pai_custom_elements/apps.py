from django.apps import AppConfig


class DjangocmsPaiCustomElementsConfig(AppConfig):
    name = 'djangocms_pai_custom_elements'
